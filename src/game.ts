export interface Frame {
  firstPipe: PipePair;
  secondPipe: PipePair;
  bird: Bird;

  gameOver: boolean;
  gameStarted: boolean;
  width: number;
  height: number;
  score: number;
  ground: Ground;
}

export interface PipePair {
  topPipe: Pipe;
  bottomPipe: Pipe;
  left: number;
  width: number;
  show: boolean;
}

export interface Pipe {
  top: number;
  height: number;
}

export interface Ground {
  height: number;
}

export interface Bird {
  top: number;
  left: number;
  size: number;
}

export class GameController {
  private frame: Frame;
  private veolcity = 0;

  constructor(
    public readonly height = 800,
    public readonly width = 400,
    public readonly pipeWidth = 50,
    public readonly pipeGap = 150,
    public readonly minTopForTopPipe = 70,
    public readonly maxTopForTopPipe = 350,
    public readonly generateNewPipePercent = 0.7,
    public readonly speed = 1,
    public readonly groundHeight = 20,
    public readonly birdX = 40,
    public readonly birdSize = 20,
    public readonly gravity = 2.25,
    public readonly jumpVelocity = 10,
    public readonly slowVelocityBy = 0.3,
  ) {}

  public newGame() {
    let firstPipe = this.createPipePair(true);
    let secondPipe = this.createPipePair(false);

    this.frame = {
      firstPipe: firstPipe,
      secondPipe: secondPipe,

      score: 0,
      width: this.width,
      height: this.height,
      gameOver: false,
      gameStarted: false,
      bird: {
        left: this.birdX,
        top: this.height / 2 - this.birdSize / 2,
        size: this.birdSize,
      },
      ground: {
        height: this.groundHeight,
      },
    };

    return this.frame;
  }

  public nextFrame() {
    // Stop "animating" in the event of a game over.
    if (this.frame.gameOver || !this.frame.gameStarted) {
      return this.frame;
    }

    this.frame.firstPipe = this.movePipe(
      this.frame.firstPipe,
      this.frame.secondPipe,
    );

    this.frame.secondPipe = this.movePipe(
      this.frame.secondPipe,
      this.frame.firstPipe,
    );

    if (this.veolcity > 0) {
      this.veolcity -= this.slowVelocityBy;
    }

    let nextBirdTop = this.frame.bird.top + this.gravity - this.veolcity;
    let maxBirdTop = (this.height - this.groundHeight - this.birdSize);

    // Test for pipe collision
    if (
      this.hasCollidedWithPipe(
        this.frame.firstPipe,
        this.frame.secondPipe,
        this.frame.bird,
      )
    ) {
      this.frame.gameOver = true;
      return this.frame;
    }

    // Test for ground collision
    if (this.frame.bird.top >= maxBirdTop) {
      this.frame.bird.top = maxBirdTop;
      this.frame.gameOver = true;
    } else {
      this.frame.bird.top = Math.min(nextBirdTop, maxBirdTop);
    }

    if (
      ((this.frame.firstPipe.left + this.pipeWidth) ==
        (this.birdX - this.speed)) ||
      ((this.frame.secondPipe.left + this.pipeWidth) ==
        (this.birdX - this.speed))
    ) {
      this.frame.score += 1;
    }

    return this.frame;
  }

  public start() {
    this.newGame();
    this.frame.gameStarted = true;
    return this.frame;
  }

  public jump() {
    if (this.veolcity <= 0) {
      this.veolcity += this.jumpVelocity;
    }
  }

  public hasCollidedWithPipe(
    firstPipe: PipePair,
    secondPipe: PipePair,
    bird: Bird,
  ) {
    let first_test = this.alsoCheckPipe(firstPipe, bird);
    let second_test = this.alsoCheckPipe(secondPipe, bird);

    return first_test || second_test;
  }

  public alsoCheckPipe(pipe: PipePair, bird: Bird) {
    if (pipe.show && this.checkPipe(pipe.left)) {
      return !(
        (bird.top > pipe.topPipe.height) &&
        ((bird.top + this.birdSize) < pipe.bottomPipe.top)
      );
    }
  }

  public checkPipe(left: number) {
    return (
      (left <= (this.birdX + this.birdSize)) &&
      ((left + this.pipeWidth) >= this.birdX)
    );
  }

  private randomYForTopPipe(): number {
    return (
      this.minTopForTopPipe +
      (this.maxTopForTopPipe - this.minTopForTopPipe) * Math.random()
    );
  }

  private createPipePair(show: boolean): PipePair {
    const height = this.randomYForTopPipe();

    return {
      topPipe: {
        top: 0,
        height,
      },
      bottomPipe: {
        top: height + this.pipeGap,
        height: this.height,
      },
      left: this.width - this.pipeWidth,
      width: this.pipeWidth,
      show,
    };
  }

  private movePipe(pipe: PipePair, otherPipe: PipePair) {
    // If all the way offscreen, remove it instead of moving?
    if (pipe.show && pipe.left <= (-1 * this.pipeWidth)) {
      pipe.show = false;
      return pipe;
    }

    // If not offscreen, and visible, move pipe
    if (pipe.show) {
      pipe.left -= this.speed;
    }

    // Randomly make new pipe?
    if (
      otherPipe.left < this.width * (1 - this.generateNewPipePercent) &&
      otherPipe.show && !pipe.show
    ) {
      return this.createPipePair(true);
    }

    return pipe;
  }
}
